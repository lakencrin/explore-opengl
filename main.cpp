#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <glad/glad.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <fstream>
#include <sstream>

std::string readAll(const std::string& filename)
{
	std::ifstream vertex_shader{ filename, std::ios_base::in };
	if (not vertex_shader.is_open())
		return std::string{};
	std::stringstream vshader_source;
	vshader_source << vertex_shader.rdbuf();
	return vshader_source.str();
}
void shaderSource(const std::string& filename, GLuint shader)
{
	std::string vshader_source = ::readAll(filename);
	const char* vshader_source_ptr = vshader_source.c_str();

	glShaderSource(shader, 1, &vshader_source_ptr, nullptr);
}
GLint createProgram()
{
	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	shaderSource("vertex_shader.glsl", vshader);
	glCompileShader(vshader);

	int success;
	glGetShaderiv(vshader, GL_COMPILE_STATUS, &success);

	GLint fshader = glCreateShader(GL_FRAGMENT_SHADER);
	shaderSource("fragment_shader.glsl", fshader);
	glCompileShader(fshader);
	glGetShaderiv(fshader, GL_COMPILE_STATUS, &success);
	char infolog[512];
	glGetShaderInfoLog(fshader, 512, nullptr, infolog);

	GLint program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glLinkProgram(program);
	glUseProgram(program);
	glDeleteShader(vshader);
	glDeleteShader(fshader);

	return program;
}


void processEvents(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

int main()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	int const width = 600;
	int const height = 300;

	GLFWwindow* window = glfwCreateWindow(width, height, u8"Хер", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	if (not gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		return 1;

	GLint program = createProgram();

	float vertices_color[] = {
			// positions          // colors           // texture coords
			 0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
			 0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
			-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
			-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
		};

	int indices[] = {
		0, 1, 3,
		1, 2, 3
	};


	GLuint array_object;
	glGenVertexArrays(1, &array_object);

	GLuint buffer_object;
	glGenBuffers(1, &buffer_object);
	GLuint elements_object;
	glGenBuffers(1, &elements_object);

	glBindVertexArray(array_object);

	glBindBuffer(GL_ARRAY_BUFFER, buffer_object);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_color), vertices_color, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	stbi_set_flip_vertically_on_load(true);
	GLuint texture1;
	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	int iidth, iheight, channels;
	unsigned char* image_data1 = stbi_load("container.jpg", &iidth, &iheight, &channels, 0);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, iidth, iheight, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data1);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image_data1);

	GLuint texture2;
	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int iidth2, iheight2, channels2;
	stbi_uc* image_data2 = stbi_load("awesomeface.png", &iidth2, &iheight2, &channels2, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, iidth2, iheight2, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data2);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image_data2);

	glViewport(0, 0, width, height);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture2);
	glUniform1i(glGetUniformLocation(program, "Texture1"), 0);
	glUniform1i(glGetUniformLocation(program, "Texture2"), 1);

	while (not glfwWindowShouldClose(window))
	{
		processEvents(window);

		glClearColor(0.0f, 1.0f, 0.1f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(program);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);

		glBindVertexArray(array_object);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}