#include "Shader.h"

#include <fstream>
#include <sstream>

Shader::Shader(GLenum shader_type, const std::string& shader_name)
	: _id{ glCreateShader(shader_type) }
	, _type{ shader_type }
{
	std::string const source = readFile(shader_name);
	const char* source_str = source.c_str();
	glShaderSource(_id, 1, &source_str, nullptr);
	glCompileShader(_id);
}

/*static*/ std::string Shader::readFile(const std::string& filename)
{
	std::ifstream vertex_shader{ filename, std::ios_base::in };
	if (not vertex_shader.is_open())
		return std::string{};
	std::stringstream vshader_source;
	vshader_source << vertex_shader.rdbuf();
	return vshader_source.str();
}