#version 330 core
in vec4 verColor;
in vec2 TextureCoord;

out vec4 FragColor;

uniform sampler2D Texture1;
uniform sampler2D Texture2;

void main()
{
	FragColor = mix(texture(Texture1, TextureCoord), texture(Texture2, TextureCoord), 0.2) * verColor;
}