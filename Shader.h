#pragma once

#include <glad/glad.h>

#include <string>

class Shader
{
	unsigned int _id;
	GLenum _type;

public:
	Shader(GLenum shader_type, const std::string& shader_name);
	~Shader() = default;

	unsigned int id() const { return _id; }
	GLenum type() const { return _type; }

private:
	static std::string readFile(const std::string& filename);
};